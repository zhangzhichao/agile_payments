<?php
require 'common.php';

//下单
function miniOrder(){
    $msgSrcId='1017';   //来源id
    $data=[];
    $data['requestTimestamp']=date("YmdHis",time());    // 报文请求时间
    $data['mid'] ='898201612345678'; // 商户号
    $data['tid'] ='88880001';    // 终端号
    $data['merOrderId']=getMerOrderId($msgSrcId);
    $data['instMid'] ='MINIDEFAULT'; // 业务类型
    $data['orderDesc'] ='账单描述';  // 订单描述 展示在支付截图中
    $data['totalAmount']= 2;      // 支付总金额
    $data['subAppId']= 'wx0bd72821b0ce53cb';      // 微信必填
    $data['subOpenId']= 'o4Sic5HPuB3j-LmnQTVIC4G_oYqY';      // 微信必填  前端获取用户的openid 传给后台

    $data['tradeType']= 'MINI';
    //分账
   /*
    $subOrders=[];
    $sub['totalAmount']= 1;      // 支付子金额
    $sub['mid']= "898127210280001";      //
    $sub1['totalAmount']= 1;      // 支付子金额
    $sub1['mid']= "988460101800201";      //
    $subOrders[]=$sub;
    $subOrders[]=$sub1;
    $data['divisionFlag']= 'true'; //分账标识
    $data['subOrders']= $subOrders;      //*/

    $body=json_encode($data);
    echo "请求报文：".$body."<br/>";

    $url='https://test-api-open.chinaums.com/v1/netpay/wx/unified-order';
    //$url='https://api-mop.chinaums.com/v1/netpay/wx/unified-order';  //生产环境地址

    curlpost($url,$body);
}


//查询
function query(){
    $msgSrcId='1017';   //来源id
    $data=[];
    $data['requestTimestamp']=date("YmdHis",time());    // 报文请求时间
    $data['mid'] ='898201612345678'; // 商户号
    $data['tid'] ='88880001';    // 终端号
    $data['merOrderId']='101720220303143314904287';
    //$data['merOrderId']='101720220223102617810382';
    $data['instMid'] ='MINIDEFAULT'; // 业务类型

    $body=json_encode($data);
    echo "请求报文：".$body."<br/>";

    $url='https://test-api-open.chinaums.com/v1/netpay/query';
    //$url='https://api-mop.chinaums.com/v1/netpay/trade/app-pre-query';  //生产环境地址
    curlpost($url,$body);
}




miniOrder();

//query();