<?php
require 'common.php';

//下单
function appOrder(){
    $msgSrcId='1017';   //来源id
    $data=[];
    $data['requestTimestamp']=date("YmdHis",time());    // 报文请求时间
    $data['mid'] ='898201612345678'; // 商户号
    $data['tid'] ='88880001';    // 终端号
    $data['merOrderId']=getMerOrderId($msgSrcId);
    $data['instMid'] ='APPDEFAULT'; // 业务类型
    $data['orderDesc'] ='账单描述';  // 订单描述 展示在支付截图中
    $data['totalAmount']= 2;      // 支付总金额
    $data['subAppId']= 'wx0bd72821b0ce53cb';      // 微信必填

    $data['tradeType']= 'APP';      // 支付总金额
    //分账
   /*
    $subOrders=[];
    $sub['totalAmount']= 1;      // 支付子金额
    $sub['mid']= "898127210280001";      //
    $sub1['totalAmount']= 1;      // 支付子金额
    $sub1['mid']= "988460101800201";      //
    $subOrders[]=$sub;
    $subOrders[]=$sub1;
    $data['divisionFlag']= 'true'; //分账标识
    $data['subOrders']= $subOrders;      //*/

    $body=json_encode($data);
    echo "请求报文：".$body."<br/>";

    $url='https://test-api-open.chinaums.com/v1/netpay/trade/precreate';
    //$url='https://api-mop.chinaums.com/v1/netpay/trade/app-pre-order';  //生产环境地址
    curlpost($url,$body);
}


//查询
function query(){
    $data=[];
    $data['requestTimestamp']=date("YmdHis",time());    // 报文请求时间
    $data['mid'] ='898201612345678'; // 商户号
    $data['tid'] ='88880001';    // 终端号
    $data['merOrderId']='101720220303143314904287';
    //$data['merOrderId']='101720220223102617810382';
    $data['instMid'] ='APPDEFAULT'; // 业务类型

    $body=json_encode($data);
    echo "请求报文：".$body."<br/>";

    $url='https://test-api-open.chinaums.com/v1/netpay/trade/app-pre-query';
    //$url='https://api-mop.chinaums.com/v1/netpay/trade/app-pre-query';  //生产环境地址
    curlpost($url,$body);
}




//appOrder();

query();