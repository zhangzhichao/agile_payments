<?php
require 'h5_common.php';

$msgSrcId='1017';

function pay(){
    global $msgSrcId;

    $data=[];
    $data['requestTimestamp']=date("YmdHis",time());    // 报文请求时间
    $data['mid'] ='898310148160568'; // 商户号
    $data['tid'] ='88880001';    // 终端号
    $data['merOrderId']=getMerOrderId($msgSrcId);
    $data['instMid'] ='H5DEFAULT'; // 业务类型
    $data['totalAmount']= 1;      // 支付总金额

    $body=json_encode($data);
    echo "请求报文：".$body."\r\n";

    $url='https://test-api-open.chinaums.com/v1/netpay/trade/h5-pay';
    //$url='https://api-mop.chinaums.com/v1/netpay/trade/h5-pay';
    curlget($url,$body);
}

function query(){

    $data=[];
    $data['requestTimestamp']=date("YmdHis",time());    // 报文请求时间
    $data['mid'] ='898310148160568'; // 商户号
    $data['tid'] ='88880001';    // 终端号
    $data['merOrderId']='101720210712182110161543';  //需要查询的订单号
    $data['instMid'] ='H5DEFAULT'; // 业务类型
    //$data['totalAmount']= 1;      // 支付总金额

    $body=json_encode($data);
    echo "请求报文：".$body."\r\n";

    $url='https://test-open.chinaums.com/v1/netpay/query';
    curlpost($url,$body);
}




pay();
//query();