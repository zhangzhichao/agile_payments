<?php
require 'h5_common.php';
//接收支付通知，并验签
$params = array_map(function($value){
    return urldecode($value);
}, $_POST);
//file_put_contents('log.txt', "通知内容:".json_encode($params)."\r\n", FILE_APPEND);
console("通知内容:".json_encode($params)."\r\n");
$md5Key='impARTxrQcfwmRijpDNCw6hPxaWCddKEpYxjaKXDhCaTCXJ6';    //通知验签密钥 生产环境请记得更换

$sign = makeSign($md5Key, $params);

console("自己计算的签名:".$sign."\r\n");

$notifySign = array_key_exists('sign', $params) ? $params['sign'] : '';
if (strcmp($sign, $notifySign) == 0) {
    //处理自己业务逻辑...
    //处理完自己业务逻辑后须返回SUCCESS
    echo "SUCCESS";
} else {
    echo "UNSUCCESS";
}
