<?php date_default_timezone_set("PRC"); ?>
<?php

$appid = '银商提供';
$appkey = '银商提供';


function getSignature($timestamp,$nonce,$body){
    global $appid, $appkey;
    $str = bin2hex(hash('sha256', $body, true));
    echo "$appid$timestamp$nonce$str"."\r\n";
    $signature = base64_encode(hash_hmac('sha256', "$appid$timestamp$nonce$str", $appkey, true));
	return $signature;
}

function curlpost($url,$body){
    global $appid;
    $timestamp = date("YmdHis",time());
    $nonce = md5(uniqid(microtime(true),true));
    $signature=getSignature($timestamp,$nonce,$body);
    $authorization = "OPEN-BODY-SIG AppId=\"$appid\", Timestamp=\"$timestamp\", Nonce=\"$nonce\", Signature=\"$signature\"";
    echo $authorization."\r\n";

    $ch = curl_init();//初始化curl

    curl_setopt($ch, CURLOPT_URL, $url);//抓取指定网页
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // 从证书中检查SSL加密算法是否存在
    curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($body),
        'Authorization: '.$authorization
        ));
    $output = curl_exec($ch);//运行curl
    echo "响应报文：".$output."\r\n";
    curl_close($ch);
    return $output;

}
function curlget($url,$body){
    global $appid;

    $timestamp= date("YmdHis",time());
    $nonce = md5(uniqid(microtime(true),true));

    $signature=urlencode(getSignature($timestamp,$nonce,$body));
    //authorization=OPEN-FORM-PARAM" + "&appId=" + appId + "&timestamp=" + timestamp + "&nonce=" + nonce;
    $reqUrl=$url."?authorization=OPEN-FORM-PARAM&appId=".$appid."&timestamp=".$timestamp."&nonce=".
        $nonce."&content=".urlencode($body)."&signature=".$signature;
    echo "请求url:".$reqUrl."\r\n";
    return $reqUrl;



}



function getMerOrderId($msgSrcId){
    $billNo=$msgSrcId.date("YmdHis",time()).str_pad(mt_rand(0, 999999), 6, "0", STR_PAD_BOTH);
    return $billNo;
}

//支付通知验签
function checkSign(){
    $str="billPayment=%7B%22payTime%22%3A%222021-07-08+18%3A27%3A50%22%2C%22paySeqId%22%3A%2200962600037N%22%2C%22invoiceAmount%22%3A1%2C%22settleDate%22%3A%222021-07-08%22%2C%22buyerId%22%3A%22o8wNP0T-_KazB16xjTZLeTVCr7Mo%22%2C%22receiptAmount%22%3A1%2C%22totalAmount%22%3A1%2C%22billBizType%22%3A%22bills%22%2C%22buyerPayAmount%22%3A1%2C%22targetOrderId%22%3A%224200001194202107087742662577%22%2C%22merOrderId%22%3A%22202107081826479104325860%22%2C%22status%22%3A%22TRADE_SUCCESS%22%2C%22targetSys%22%3A%22WXPay%22%7D&qrCodeId=10002107080822653185145700&billDesc=%E5%90%88%E5%90%8C2020PAZL100764180-ZL-12%E6%94%B6%E6%AC%BE&sign=163156A3E88D164327A17FCE7D49349C&merName=%E6%B5%8B%E8%AF%95%E9%80%80%E8%B4%A75%281111%29&mid=898340149000005&billDate=2021-07-08&qrCodeType=BILLPAY&mchntUuid=4aa8728a06a04f7385869df8b659cd01&tid=88880001&instMid=QRPAYDEFAULT&receiptAmount=1&totalAmount=1&createTime=2021-07-08+18%3A26%3A53&tI=yXKN&billStatus=PAID&signType=MD5&notifyId=fc3149b9-97d3-41d8-9c76-bf758da69914&billNo=20210708182647910432586&subInst=000100&seqId=00962600037N&billQRCode=https%3A%2F%2Fqr-test3.chinaums.com%2Fbills%2FqrCode.do%3Fid%3D10002107080822653185145700";
    $str1=urldecode($str);
    $arraylist= explode("&",$str1);
    sort($arraylist);
    $md5key='impARTxrQcfwmRijpDNCw6hPxaWCddKEpYxjaKXDhCaTCXJ6';
    $signbf="";//通知中的sign
    $calSign="";
    $strForSign="";//待签串
    $signData=[];
    for($i=0;$i<count($arraylist);$i++) //把它们全部输出来
    {
        $arr=explode("=",$arraylist[$i],2);
        $signData[$arr[0]]=$arr[1];
        if($i<count($arraylist)-1){
            if($arr[0]!='sign'){
                $strForSign=$strForSign.$arraylist[$i]."&";
            }else{
                $signbf=$arr[1];
            }
        }else{
            $strForSign=$strForSign.$arraylist[$i].$md5key;
        }
    }

    if($signData['signType']=='MD5'){
        $calSign=strtoupper(md5($strForSign));
    }else{
        $calSign=strtoupper(bin2hex(hash('sha256', $strForSign, true)));
    }


    echo json_encode($signData)."\r\n";
    echo "待签名字符串：".$strForSign."\r\n";
    echo "通知中的sign:".$signbf."\r\n";
    echo "计算后的sign:".$calSign."\r\n";

    if($calSign==$signbf){
        echo "签名认证是否通过："."true";
    }else{
        echo "签名认证是否通过："."false";
    }

}



// 获取加密的参数字符串
function buildSignStr($params) {
    $keys = [];
    foreach($params as $key => $value) {
        if ($key == 'sign' || empty($value)) {
            continue;
        }
        array_push($keys, $key);
    }
    $str = '';
    sort($keys);
    $len = count($keys);
    for($i = 0; $i < $len; $i++) {
        $v = $params[$keys[$i]];
        if (is_array($v)) {
            $v = json_encode($v);
        }
        $str .= $keys[$i] . '=' . $v . (($i === $len -1) ? '' : "&");
    }
    return $str;
}

function console($data){
    $stdout = fopen('php://stdout', 'w');
    fwrite($stdout,$data."\n");
    fclose($stdout);
}


