<?php

namespace AgilePayments;

use app\api\controller\UMFintech;
use Exception;
use Alipay\EasySDK\Kernel\Factory;
use Alipay\EasySDK\Kernel\Util\ResponseChecker;
use AgilePayments\Config;

/**
 * @method pay      pay     支付方法
 * @method query    query   查询方法
 * @method refund   refund  退款方法
 * @method refundQ  refundQ 退款查询方法
 * @method close    close   关闭支付方法
 */
class Pay
{
    private $pay_config;

    private $platform;

    private $obj;
    
    public function __construct(Config $config)
    {
        $arr = [
            'umf'       => 'UMFintech',     // 联动
            'union'     => 'Union',         // 云闪付
            'ump'       => 'Ump',           // 银商
            'alipay'    => 'Alipay',        // 支付宝
            'wechat'    => 'Wechat',        // 微信
        ];
        $this->platform = $arr[$config->pay_type_config->platform];
        $this->pay_config = $config;
    }

    public function __call($name, $arguments)
    {
        $class = "\AgilePayments\\{$this->platform}\Service";
        if (method_exists($class,$name)){
            return call_user_func([new $class,$name],$arguments);
        }else{
            throw new \Exception('调用方法错误！');
        }
    }
}