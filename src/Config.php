<?php
namespace AgilePayments;

use AgilePayments\Tool;
use \think\Db;
use think\Exception;

class Config
{
    public $frame_name;

    public $pay_type;

    private $support_framework = ['think','laravel']; // TODO  后期支持多框架 先用thinkPHP 赶时间

    public $db;  // TODO  后期做框架适配

    public $pay_type_obj;

    public $base_url = 'https://b2b.umfintech.com/';

    public $notify_url;

    public function __construct($pay_type,$configId = '')
    {
        if (class_exists('\think\facade\Db')){
            $this->db = new \think\facade\Db();
        }else{
            $this->db = new \think\Db();
        }
        $this->notify_url  = Tool::env('UMFintechPay.notifyHost');
        $this->pay_type = $pay_type;

        $this->pay_type_config = $this->getPayConfig($configId);
    }


    private function getPayConfig($configId = ''){
        try {
            if ($configId != ''){
                $pay_config = $this->db::name('agile_pay_type_config')
                    ->alias('ac')
                    ->field('ac.*,at.platform')
                    ->join('agile_pay_type at','at.config_id = ac.id',"LEFT")
                    ->where([
                        'ac.id'   => $configId,
                        'at.name'   => $this->pay_type,
                    ])->find();
            }else{
                $pay_config = $this->db::name('agile_pay_type_config')
                    ->alias('ac')
                    ->field('ac.*,at.platform')
                    ->join('agile_pay_type at','at.config_id = ac.id',"LEFT")
                    ->where([
                        'at.is_use'    => '1',
                        'at.name'   => $this->pay_type,
                    ])->find();
            }
            if ($pay_config){
                return (object)$pay_config;
            }else{
                throw new \Exception("未获发现 $this->pay_type 支付方式配置！");
            }
        }catch (\Exception $exception){
            throw new \Exception('获取支付配置失败：'.$exception->getMessage());
        }

    }

}