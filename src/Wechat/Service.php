<?php

namespace AgilePayments\Union;

use AgilePayments\Config;

class Service
{
    private $config;
    
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * 支付
     * @param string    $notify_url
     * @param string    $order_no
     * @param int       $pay_price
     * @param string    $out_time
     * @param string    $extra_param
     * @return void
     */
    public function pay($notify_url,$order_no,$pay_price,$out_time,$extra_param)
    {
        $params = array(
            //以下信息非特殊情况不需要改动
            'version' => SDKConfig::getSDKConfig()->version,                 //版本号
            'encoding' => 'utf-8',				  //编码方式
            'txnType' => '01',				      //交易类型
            'txnSubType' => '01',				  //交易子类
            'bizType' => '000201',				  //业务类型
            'frontUrl' =>  SDKConfig::getSDKConfig()->frontUrl,  //前台通知地址
            'backUrl' => "{$_SERVER['HTTP_HOST']}$this->notifyUrl",	  //后台通知地址
            'signMethod' => SDKConfig::getSDKConfig()->signMethod,	              //签名方法
            'channelType' => '08',	              //渠道类型，07-PC，08-手机
            'accessType' => '0',		          //接入类型
            'currencyCode' => '156',	          //交易币种，境内商户固定156
            'payTimeout' => date('YmdHis',time() + $this->expireTime - 10),
            'merId' => $this->yl_mid,
            'reqReserved' => json_encode($this->reqReserved),
            'orderId' => $out_order_id,
            'txnTime' => date('YmdHis'),
            'txnAmt' => $this->inData['totalAmount'],	//交易金额，单位分
        );
    }

}