<?php

namespace AgilePayments\UMFintech;

use AgilePayments\Config;
use AgilePayments\Tool;


class Member
{
    private $mer_id;

    private $pay_config;

    private $base_url;

    public function __construct(Config $config)
    {
        $this->pay_config = $config->pay_type_config;

        $this->mer_id = $this->pay_config->mid;

        $this->base_url = $config->base_url;
    }

    /**
     * @desc 余额查询
     */
    public function balance($user_id,$is_mer = false)
    {
        $_url  = $this->base_url . 'merAccess/member/balance';
        $data = [
            'mer_id'        => $this->mer_id,
            'user_id'       => $user_id,
            'version'       => '1.0',

        ];
        if ($is_mer){
            $data['user_id'] = $user_id;
        }
        return json_decode(Util::httpGet($data,$_url),1);
    }
    /**
     * @desc 账户余额提现
     */
    public function balanceWithdrawal($order_id, $user_id, $amount, $remark,$p_agreement_id, $notify_url = '', $mobile_id = '')
    {
        $_url  = $this->base_url . 'merAccess/withdrawal/balanceWithdrawal';
        $data = [
            'mer_id'            => $this->mer_id,
            'version'           => '1.0',
            'order_id'          => $order_id,
            'mer_date'          => date('Ymd'),
            'user_id'           => $user_id,
            'amount'            => $amount,
            'remark'            => $remark,
            'p_agreement_id'    => $p_agreement_id,
            'notify_url'        => $notify_url,
            'mobile_id'         => $mobile_id,

        ];
        return json_decode(Util::httpPost($data,$_url),1);
    }

    /**
     * @desc 查询提现交易
     */
    public function withdrawalOrderInfo($order_id)
    {
        $_url  = $this->base_url . 'merAccess/withdrawal/orderInfo';
        $data = [
            'mer_id'            => $this->mer_id,
            'version'           => '1.0',
            'order_id'          => $order_id,
            'mer_date'          => date('Ymd'),

        ];
        return json_decode(Util::httpGet($data,$_url),1);
    }

    /**
     * @deac 账户流水查询
     * @param $user_id int 联动侧user_id
     * @param $type int 查询类型 【1：子商户在途流水，2:子商户余额流水】
     * @param $page
     * @param $limit
     * @param $startDate string 查询时间范围（一年内，30天数据）
     * @param $endDate string 查询时间范围（一年内，30天数据）
     */
    public function tradingQuery($user_id, $type = '1', $page = 1, $limit = 10, $startDate = '', $endDate = '')
    {
        $startDate  = $startDate != ''?$startDate:date('Ymd');
        $endDate    = $endDate != ''?$endDate:date('Ymd');

        if ($type == 1){
            $acc_type = '105';
        }else{
            $acc_type = '108';
        }

        $_url  = $this->base_url . 'merAccess/member/tradingQuery';
        $data = [
            'mer_id'        => $this->mer_id,
            'version'       => '1.1',
            'amt_flag'      => '1',
            'user_id'       => $user_id,
            'acc_type'      => $acc_type,
            'start_date'    => $startDate,
            'end_date'      => $endDate,
            'page_num'      => $page,
            'page_size'     => $limit,
        ];
        return json_decode(Util::httpGet($data,$_url),true);
    }

    /**
     * 生成结算对账文件
     * @param string $mer_date 对账日期，格式为YYYYMMDD
     * @return mixed
     */
    public function settleFile($mer_date = '')
    {
        if ($mer_date == ''){
            $mer_date = date('Ymd');
        }
        $_url  = $this->base_url . 'merAccess/file/download/settleFile';
        $data = [
            'mer_id'        => $this->mer_id,
            'version'       => '1.0',
            'mer_date'      => $mer_date,
        ];
        return Util::httpGet($data,$_url,true);
    }
}