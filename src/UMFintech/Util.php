<?php
namespace AgilePayments\UMFintech;

use GuzzleHttp\Client;
use AgilePayments\Tool;
use think\Exception;
use think\Log;

class Util
{

    private static $priv_key_file = './pay_certs/UMFintech/51328.key.pem';
    private static $public_key_path = './pay_certs/UMFintech/umpay.cert.pem';
//     private static $priv_key_file = './../cert/UMFintech/90000002.key.pem';
//     private static $public_key_path = './../cert/UMFintech/test.umpay.cert.pem';

    /**
     * 私钥-RSA签名
     * @param $data
     * @param $merId
     * @return string
     */
    public static function  rsaSign($data) {
//        $private_key_path = "../cert/".$merId.".key.pem";

        $priv_key_file = Tool::getRootPath() . self::$priv_key_file;
        $data = utf8_encode($data);
        $priKey = file_get_contents($priv_key_file);
        $res = openssl_get_privatekey($priKey);
        openssl_sign($data, $sign, $res,OPENSSL_ALGO_SHA1);
        openssl_free_key($res);
        //base64编码
        $sign = base64_encode($sign);
        return $sign;
    }


    /**
     * 联动公钥验证签名
     * @param $plain
     * @param $sign
     * @return bool
     */
    public static function verifySign($plain, $sign) {
//        $public_key_path= "../cert/umpay.cert.pem";
        $public_key_path= Tool::getRootPath() . self::$public_key_path;
        $pubKey = file_get_contents($public_key_path);
        $pkeyid = openssl_get_publickey($pubKey);
        $result = (bool)openssl_verify($plain, base64_decode($sign), $pkeyid);
        openssl_free_key($pkeyid);
        if($result == true){
        }else{
        }
        return $result ? true : false;

    }




    /**
     * @param $plain		string 签名明文串
     * @param $merId		string 商户号
     * @return bool|string	签名base64编码后的字符串
     */
    public static function sign256($plain,$merId){
        try{

//			$priv_key_file = "../cert/".$merId.".key.pem";
            $priv_key_file = MER_PRIVATE_KEY_PATH;
            if(!file_exists($priv_key_file)){
                die("[UMF-BSP SDK]传入的私钥文件不存在,私钥文件绝对路径 ".$priv_key_file);
            }
            $fp = fopen($priv_key_file,"rb");
            $priv_key = fread($fp, 8192);
            @fclose($fp);
            $pkeyid = openssl_get_privatekey($priv_key);
            if(!is_resource($pkeyid)){ return FALSE;}
            // compute signature
            @openssl_sign($plain, $signature, $pkeyid, OPENSSL_ALGO_SHA256);
            // free the key from memory
            @openssl_free_key($pkeyid);
            return base64_encode($signature);
        }catch(Exception $e){
            die("[UMF-BSP SDK]签名失败".$e->getMessage());
        }
    }

    /**
     * @param $plain		string 验签明文
     * @param $signature	string 验签密文
     * @return bool			验签结果
     */
    public static function verify($plain,$signature){
//		$cert_file = "../cert/umpay.cert.pem";
        $cert_file = UMF_PUBLIC_KEY_PATH;
        if(!file_exists($cert_file)){
            die("[UMF-BSP SDK]传入的公钥文件不存在,公钥文件绝对路径 ".$cert_file);
        }
        $signature = base64_decode($signature);
        $fp = fopen($cert_file,"r");
        $cert = fread($fp, 8192);
        fclose($fp);
        $pubkeyid = openssl_get_publickey($cert);
//	    if(!is_resource($pubkeyid)){return FALSE;}
        $ok = openssl_verify($plain,$signature,$pubkeyid);
        @openssl_free_key($pubkeyid);
        if ($ok == 1) {
            return true;
        } elseif ($ok == 0) {
            return false;
        } else {
            return false;
        }
    }

    public static function verify256($plain,$signature){
        $signature = base64_decode($signature);
//		$cert_file = "../cert/umpay.cert.pem";
        $cert_file = UMF_PUBLIC_KEY_PATH;
        $fp = fopen($cert_file,"r");
        $cert = fread($fp, 8192);
        //$cert = UMF_PUBLIC_KEY;
        fclose($fp);
        $pubkeyid = openssl_get_publickey($cert);
        if(!is_resource($pubkeyid)){return FALSE;}
        $ok = openssl_verify($plain,$signature,$pubkeyid,OPENSSL_ALGO_SHA256);
        @openssl_free_key($pubkeyid);
        if ($ok == 1) {
            return true;
        } elseif ($ok == 0) {
            return false;
        } else {
            return false;
        }
    }


////////////////////////////////////////////////////////////////////jsonUtil/////////////////////////////////////////
    private static function arrayToJson($paramsArr=array()){
        ksort($paramsArr);
        return json_encode($paramsArr);
//        $map = new HashMap();
//        foreach($paramsArr as $key => $value){
//            $map->put($key,$value);
//        }
//        $paramter = array();
//        $keys = $map->keys();
//        foreach ($keys as $key) {
//            $paramter[$key."=".trim($map->get($key))] = "\"".$key."\":\"".trim($map->get($key))."\",";
//        }
//        //字母a到z排序后的数组
//        $sort_array = StringUtil::arg_sort($paramter);
//        $json = "";
//        while (list ($key, $val) = each($sort_array)) {
//            $json .= $val;
//        }
//        $json = substr($json, 0, count($json) - 2); //去掉最后一个 , 字符
//        $json = "{".$json."}";
//        return $json;
    }

////////////////////////////////////////////////////////////////////jsonUtilEND/////////////////////////////////////////




////////////////////////////////////////////////////////////////////SignUtil/////////////////////////////////////////

    /**
     * @param $plain		string 签名明文串
     * @return bool|string	签名base64编码后的字符串
     */
    public static function sign($plain){

        try{
            $priv_key_file = Tool::getRootPath() . self::$priv_key_file;
            if(!file_exists($priv_key_file)){
                Tool::log("传入的私钥文件不存在,私钥文件绝对路径 ".$priv_key_file,'error');
                return false;
            }
            $fp = fopen($priv_key_file,"rb");
            $priv_key = fread($fp, 8192);
            @fclose($fp);
            $pkeyid = openssl_get_privatekey($priv_key);
            if(!is_resource($pkeyid)){ return false;}
            @openssl_sign($plain, $signature, $pkeyid);
            @openssl_free_key($pkeyid);
            return base64_encode($signature);
        }catch(Exception $e){
            Tool::log("签名失败 | ".$e->getMessage(),'error');
            return false;
        }


//        //        $log = new Logger();
//        try{
//
////            $priv_key_file = "../cert/".$merId.".key.pem";
//            $priv_key_file = MER_PRIVATE_KEY_PATH;
////            Tool::log("[UMF-BSP SDK]私钥文件绝对路径 ".$priv_key_file);
//            if(!file_exists($priv_key_file)){
////                Tool::log("[UMF-BSP SDK]传入的私钥文件不存在,私钥文件绝对路径 ".$priv_key_file);
////                die("[UMF-BSP SDK]传入的私钥文件不存在,私钥文件绝对路径 ".$priv_key_file);
//            }
//            $fp = fopen($priv_key_file,"rb");
//            $priv_key = fread($fp, 8192);
//            @fclose($fp);
//            $pkeyid = openssl_get_privatekey($priv_key);
//            if(!is_resource($pkeyid)){ return FALSE;}
//            // compute signature
//            @openssl_sign($plain, $signature, $pkeyid);
//            // free the key from memory
//            @openssl_free_key($pkeyid);
////            Tool::log("[UMF-BSP SDK]签名前明文 " .$plain);
////            Tool::log("[UMF-BSP SDK]签名后密文 " .base64_encode($signature));
//            return base64_encode($signature);
//        }catch(Exception $e){
//            die("[UMF-BSP SDK]签名失败".$e->getMessage());
//        }
    }

    /**
     * 使用公钥对明文字符串进行加密
     * @param $data 	string 明文
     * @return string   string 密文
     */
    public static function encrypt($data) {
        
//		$cert_file = "../cert/umpay.cert.pem";
        $cert_file = Tool::getRootPath() . self::$public_key_path;
         Tool::log("[UMF-BSP SDK]公钥文件绝对路径 ".$cert_file);
        if(!file_exists($cert_file)){
             Tool::log("[UMF-BSP SDK]传入的公钥文件不存在,公钥文件绝对路径 ".$cert_file);
            die("[UMF-BSP SDK]传入的公钥文件不存在,公钥文件绝对路径 ".$cert_file);
        }
        $fp = fopen($cert_file,"r");
        $public_key = fread($fp,8192);
        //$public_key = UMF_PUBLIC_KEY;
        //fclose($fp);
        $public_key = openssl_get_publickey($public_key);
        openssl_public_encrypt($data,$crypttext,$public_key);
        $encryptDate = base64_encode($crypttext);
        // Tool::log ( "[UMF-BSP SDK]敏感信息加密前明文: " . $data);
         Tool::log ( "[UMF-BSP SDK]敏感信息加密后密文: " . $encryptDate);
        return $encryptDate;
    }

    /**
     * @param $data		string 密文
     * @return string	string 明文
     */
    public static function decrypt($data) {
        
//		$priv_key_file = "../cert/".$merId.".key.pem";
        $priv_key_file = Tool::getRootPath() . self::$priv_key_file;
         Tool::log("[UMF-BSP SDK]私钥文件绝对路径 ".$priv_key_file);
        if(!file_exists($priv_key_file)){
             Tool::log("[UMF-BSP SDK]传入的私钥文件不存在,私钥文件绝对路径 ".$priv_key_file);
            die("[UMF-BSP SDK]传入的私钥文件不存在,私钥文件绝对路径 ".$priv_key_file);
        }
        $fp = fopen($priv_key_file,"r");
        $private_key = fread($fp,8192);
        fclose ($fp);
        $private_key = openssl_get_privatekey($private_key);
        openssl_private_decrypt(base64_decode($data), $decrypted, $private_key);
        $decryptDate =iconv("UTF-8", "UTF-8", $decrypted);
         Tool::log ( "[UMF-BSP SDK]敏感信息解密前密文: " . $data);
         Tool::log ( "[UMF-BSP SDK]敏感信息解密后明文: " . $decryptDate);
        return $decryptDate;
    }



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function httpGet($data,$url="",$noSign = false) {

        ksort($data);
        $str = self::createLinkstringUrlencode($data);
        $sign = self::sign($str);
        if ($sign === false){
            Tool::log('\AgilePayments\UMFintech\Util::httpGet | 签名错误 ', 'error');
            return false;
        }
        $headers = array(
            "Content-Type: application/json",
            "Signature:" . $sign
        );
        $url  =   $url . '?' . $str;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT,90);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
        $output = curl_exec($ch);

        if (curl_errno($ch)){
            Tool::log('[UMF-BSP SDK]网络请求失败,错误信息 = '.curl_error($ch),'error');
            return false;
        }
        Tool::log("HTTP_GET_START",'info');
        Tool::log("本次请求原始数据 " . PHP_EOL . " URL: ". $url . PHP_EOL . " Header: " . json_encode($headers),'info');
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == '200') {
            $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($output, 0, $headerSize);
            $body = substr($output, $headerSize);
        }
        curl_close($ch);
        if ($noSign){
            $resData = $body;
        }else{
            $resData = self::parseResponse($header,$body);
        }
        Tool::log("本次请求验签后数据 ".$resData);
        Tool::log("HTTP_GET_END",'info');
        return $resData;


//        if(array_key_exists("Signature",$headers)){
//            $resSign = $headers['Signature'];
//            Tool::log("[UMF-BSP SDK]响应sign " .$resSign);
//        }

    }

    /**
     * @param string $urlStr 向服务端发起POST请求的url字符串
     * @param $map
     */
    public static function httpPost($data,$url=""){
        //json报文
        ksort($data);
        $jsonData = self::arrayToJson($data);

        //获取签名
        $sign = self::sign($jsonData);

        $headers = array(
            "Content-Length: " . strlen($jsonData),
            "Content-Type: application/json",
            "Signature: " . $sign
        );

        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL, $url);
        // 对认证证书来源的检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // 从证书中检查SSL加密算法是否存在
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonData);
        // 设置超时限制防止死循环
        curl_setopt($ch, CURLOPT_TIMEOUT,90);
//        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_HEADER, TRUE);    //表示需要response header
//        curl_setopt($ch, CURLOPT_NOBODY, TRUE); //表示需要response body
        curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);

        $response = curl_exec($ch);//运行curl

        Tool::log("HTTP_POST_START",'info');
        Tool::log("本次请求原始数据 " . PHP_EOL . " URL: ". $url . PHP_EOL . " Header: " . json_encode($headers) . PHP_EOL . " Data: ". $jsonData ,'info');
        if (curl_errno($ch)){
            Tool::log('[UMF-BSP SDK]网络请求失败,错误信息 = '.curl_error($ch));
            die('[UMF-BSP SDK]网络请求失败,错误信息 = '.curl_error($ch));//捕抓异常
        }
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == '200') {
//            $statusCode = curl_getinfo($ch,CURLINFO_HTTP_CODE);
            $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($response, 0, $headerSize);
            $body = substr($response, $headerSize);
            $resData = self::parseResponse($header,$body);
        }else{
            $resData = json_encode(explode(PHP_EOL,$response));
        }
        
        curl_close($ch);

        Tool::log("本次请求验签后数据 ".$resData);
        Tool::log("HTTP_POST_END",'info');

        return $resData;

    }




    private static function createLinkstringUrlencode($para){
        $arg  = '';
        foreach($para as $k=>$v){
            $arg .= $k.'='.urlencode($v).'&';
        }
        //去掉最后一个&字符
        $arg = substr($arg, 0, -1);

        //如果存在转义字符,那么去掉转义
//        if(get_magic_quotes_gpc()){
//            $arg = stripslashes($arg);
//        }

        return $arg;
    }


    /**
     * 解析响应报文并验签
     * @param $header
     * @param $body
     * @return mixed
     */
    private static function parseResponse($header,$body){
        if(!empty($header)) {
            preg_match('/Signature\W\W([\w\+\=\/]*)/i', $header, $matches);
            if (!empty($matches)) {
                $resSign = $matches[1];
            }
        }
        Tool::log("响应sign " .$resSign);
        Tool::log("响应header " .$header);
        Tool::log("响应body " .$body);
//        $plain=iconv("UTF-8", "GBK", $body);
        $resf = self::verifySign($body,$resSign);
        if($resf == true) {
//            Tool::log( "本次请求原始验签成功 ");
            return $body;
        }elseif ($resf == false){
            Tool::log("平台响应数据验证签名失败",'error');
            return false;
        }else{
            Tool::log("平台响应数据验证签名发生异常",'error');
            return false;
        }

    }

    public static function bankName($gate_id = '')
    {
        $arr = [
            '银行简码'	=> '银行名称',
            'BOC'	=> '中国银行',
            'ABC'	=> '中国农业银行',
            'ICBC'	=> '中国工商银行',
            'CCB'	=> '中国建设银行',
            'COMM'	=> '交通银行',
            'PSBC'	=> '中国邮政储蓄银行',
            'CMB'	=> '招商银行',
            'CMBC'	=> '中国民生银行',
            'CITIC'	=> '中信银行',
            'CEB'	=> '中国光大银行',
            'HXB'	=> '华夏银行',
            'GDB'	=> '广发银行',
            'SDB'	=> '深圳发展银行',
            'CIB'	=> '兴业银行',
            'SPDB'	=> '浦东发展银行',
            'EGB'	=> '恒丰银行',
            'QLBANK'	=> '齐鲁银行',
            'YTB'	=> '烟台商业银行',
            'WFCCB'	=> '潍坊银行',
            'LSB'	=> '临沂商业银行',
            'CZB'	=> '浙商银行',
            'CBHB'	=> '渤海银行',
            'SHB'	=> '上海银行',
            'XMCCB'	=> '厦门银行',
            'BJB'	=> '北京银行',
            'FJHXB'	=> '福建海峡银行',
            'NBB'	=> '宁波银行',
            'SPAB'	=> '平安银行',
            'JZCB'	=> '焦作市商业银行',
            'WZCB'	=> '温州银行',
            'GCB'	=> '广州银行',
            'HKBC'	=> '汉口银行',
            'DAQINGB'	=> '龙江银行',
            'SJB'	=> '盛京银行',
            'BOLY'	=> '洛阳银行',
            'DLB'	=> '大连银行',
            'SZB'	=> '苏州市商业银行',
            'NJCB'	=> '南京银行',
            'DGB'	=> '东莞市商业银行',
            'JHCCB'	=> '金华银行',
            'URMQCCB'	=> '乌鲁木齐市商业银行',
            'SXCCB'	=> '绍兴银行',
            'BCD'	=> '成都市商业银行',
            'FSB'	=> '抚顺银行',
            'HLDB'	=> '葫芦岛市商业银行',
            'ZZB'	=> '郑州银行',
            'YCCCB'	=> '宁夏银行',
            'CRB'	=> '珠海华润银行',
            'QSB'	=> '齐商银行',
            'BOJZ'	=> '锦州银行',
            'HSB'	=> '徽商银行',
            'CQB'	=> '重庆银行',
            'HEBB'	=> '哈尔滨银行',
            'GYB'	=> '贵阳银行',
            'XAB'	=> '西安银行',
            'WRCB'	=> '无锡市商业银行',
            'DDB'	=> '丹东银行',
            'LZB'	=> '兰州银行',
            'NCB'	=> '南昌银行',
            'JSHB'	=> '晋商银行',
            'QDCCB'	=> '青岛商行',
            'JLB'	=> '吉林市商业银行',
            'JJCCB'	=> '九江银行',
            'BOAS'	=> '鞍山市商业银行',
            'QHDB'	=> '秦皇岛市商业银行',
            'QHB'	=> '青海银行',
            'TZCB'	=> '台州银行',
            'CSCB'	=> '长沙银行',
            'GZCCB'	=> '赣州银行',
            'QZCCB'	=> '泉州市商业银行',
            'BOYK'	=> '营口市商业银行',
            'FXB'	=> '阜新银行',
            'JXCCB'	=> '嘉兴市商业银行',
            'LCCB'	=> '廊坊银行',
            'ZJTLCRU'	=> '泰隆城市信用社',
            'BOIMC'	=> '内蒙古银行',
            'HZCCB'	=> '湖州市商业银行',
            'BOCZ'	=> '沧州银行',
            'WHSHB'	=> '威海市商业银行',
            'PZHCCB'	=> '攀枝花市商业银行',
            'MYSYYH'	=> '绵阳市商业银行',
            'LZCCB'	=> '泸州市商业银行',
            'DTCCB'	=> '大同市商业银行',
            'SMXB'	=> '三门峡市商业银行',
            'CJCCB'	=> '江苏长江商业银行',
            'LZHCCB'	=> '柳州银行',
            'CGNB'	=> '南充市商业银行',
            'LSBC'	=> '莱商银行',
            'TSB'	=> '唐山市商业银行',
            'LPSB'	=> '六盘水商行',
            'QJCCB'	=> '曲靖市商业银行',
            'JCCB'	=> '晋城银行',
            'JSB'	=> '江苏银行',
            'CZCCB'	=> '长治市商业银行',
            'CDB'	=> '承德银行',
            'DZB'	=> '德州银行',
            'ZYB'	=> '遵义市商业银行',
            'HDCB'	=> '邯郸市商业银行',
            'ASHB'	=> '安顺市商业银行',
            'YXCCB'	=> '玉溪市商业银行',
            'MTBANK'	=> '浙江民泰商业银行',
            'SRB'	=> '上饶银行',
            'DYCCB'	=> '东营市商业银行',
            'TACCB'	=> '泰安市商业银行',
            'CZCB'	=> '浙江稠州商业银行',
            'WHCB'	=> '乌海银行',
            'ZGB'	=> '自贡市商业银行',
            'ORBANK'	=> '鄂尔多斯银行',
            'HBSB'	=> '鹤壁银行',
            'XCCB'	=> '许昌市商业银行',
            'JNB'	=> '济宁银行',
            'BOTL'	=> '铁岭市商业银行',
            'LSCCB'	=> '乐山市商业银行',
            'CCABC'	=> '长安银行',
            'CCQTGB'	=> '重庆三峡银行',
            'SZSCCB'	=> '石嘴山银行',
            'PJB'	=> '盘锦市商业银行',
            'KLB'	=> '昆仑银行',
            'PDSCB'	=> '平顶山银行',
            'CYCB'	=> '朝阳市商业银行',
            'SNCCB'	=> '遂宁市商业银行',
            'BOBD'	=> '保定市商业银行',
            'XTB'	=> '邢台银行',
            'LSZSH'	=> '凉山州商业银行',
            'LHB'	=> '漯河商行',
            'DZCCB'	=> '达州市商业银行',
            'XXSSH'	=> '新乡市商业银行',
            'JZB'	=> '晋中市商业银行',
            'ZMDB'	=> '驻马店市商业银行',
            'HSMB'	=> '衡水市商业银行',
            'ZKB'	=> '周口市商业银行',
            'YQCCB'	=> '阳泉市商业银行',
            'YBCCB'	=> '宜宾市商业银行',
            'XJKCCB'	=> '库尔勒市商业银行',
            'YACCB'	=> '雅安市商业银行',
            'SQB'	=> '商丘商行',
            'AYB'	=> '安阳市商业银行',
            'XYCB'	=> '信阳银行',
            'HRXJB'	=> '华融湘江银行',
            'SRCB'	=> '上海农村信用社',
            'JSNXB'	=> '昆山农信社',
            'CSRCB'	=> '常熟农村商业银行',
            'GRCB'	=> '广州农村商业银行',
            'SDEB'	=> '佛山顺德农村商业银行',
            'HBXH'	=> '湖北农村信用社',
            'JRCB'	=> '江阴农村商业银行',
            'CRCB'	=> '重庆农村商业银行',
            'SDNXS'	=> '山东省农村信用社',
            'DRCB'	=> '东莞农村商业银行',
            'ZRCB'	=> '张家港农村商业银行',
            'FJNXB'	=> '福建省农村信用社',
            'BJRCB'	=> '北京农村商业银行',
            'TRCB'	=> '天津农村商业银行',
            'YZB'	=> '鄞州银行',
            'CDRCB'	=> '成都农村商业银行',
            'JSNX'	=> '江苏省农村信用社',
            'WJRCB'	=> '吴江农商行',
            'ZJRCC'	=> '浙江省农村信用社',
            'ZHRCU'	=> '珠海农村信用社',
            'TCRCB'	=> '太仓农村商业银行',
            'GZNXB'	=> '贵州省农村信用社',
            'WXRCB'	=> '无锡农村商业银行',
            'JXNXS'	=> '江西农信联合社',
            'HNNX'	=> '河南省农村信用社',
            'SXNXS'	=> '陕西省农村信用社',
            'GXRCU'	=> '广西农村信用社',
            'XJCCB'	=> '新疆农村信用社',
            'JLNLS'	=> '吉林农信联合社',
            'BYW'	=> '黄河农村商业银行',
            'AHRCU'	=> '安徽省农村信用社',
            'QHYHXH'	=> '青海省农村信用社',
            'GDRCU'	=> '广东省农村信用社',
            'NMGNXS'	=> '内蒙古农村信用社',
            'SCRCU'	=> '四川省农村信用社',
            'GSRCU'	=> '甘肃省农村信用社',
            'LNRCC'	=> '辽宁省农村信用社',
            'SXINJ'	=> '山西省农村信用社',
            'TJBHB'	=> '天津滨海农村商业银行',
            'HLJRCC'	=> '黑龙江省农村信用社',
            'QDJMB'	=> '青岛即墨北农商村镇银行',
            'ZJCURB'	=> '浙江长兴联合村镇银行',
            'EDRB'	=> '深圳龙岗鼎业村镇银行',
            'ZSXLB'	=> '中山小榄村镇银行',
            'FTYZB'	=> '深圳福田银座村镇银行',
            'HZCB'	=> '杭州市商业银行',
            'BORZ'	=> '日照银行',
            'FDB'	=> '富滇银行',
            'ZJTLCB'	=> '浙江泰隆商业银行',
            'BOBBG'	=> '广西北部湾银行',
            'GLB'	=> '桂林银行',
            'SHRCB'	=> '上海农村商业银行',
            'YNRCC'	=> '云南省农村信用社',
            'HEBNX'	=> '河北省农村信用社',
            'HBB'	=> '湖北银行',
            'GZCRU'	=> '广州农村信用合作社',
            'SZCRU'	=> '深圳市农村商业银行',
            'BOFJDZ'	=> '景德镇商业银行',
            'BOHH'	=> '新疆汇和银行',
            'GHB'	=> '广东华兴银行',
            'JNRCU'	=> '江南农村商业银行',
            'BARXB'	=> '深圳宝安融兴村镇银行',
            'NYCB'	=> '南阳村镇银行',
            'BHB'	=> '河北银行股份有限公司',
            'BSB'	=> '包商银行',
            'PYYH'	=> '濮阳银行',
            'NBCB'	=> '宁波通商银行',
            'GSB'	=> '甘肃银行',
            'SHRCB'	=> '上海农商银行',
            'QRCB'	=> '青岛农信',
            'FJNX'	=> '福建省农村信用社联合社',
            'GZNXRU'	=> '贵州省农村信用社联合社',
            'HNNXB'	=> '湖南省农村信用社联合社',
            'HNB'	=> '海南省农村信用社联合社',
            'ZJCB'	=> '广东南粤银行',
            'WXRCB'	=> '无锡农村商业银行',
            'ZHZB'	=> '枣庄银行',
            'WHRCB'	=> '武汉农村商业银行',
            'BEA'	=> '东亚银行',
        ];
        return array_key_exists($gate_id,$arr)?$arr[$gate_id]:'';
    }


}