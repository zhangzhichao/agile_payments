<?php

namespace AgilePayments\UMFintech;

use AgilePayments\Service;


class Access extends Service
{
    private $mer_id;

    public function __construct($pay_type = '')
    {
        parent::__construct($pay_type);
        $this->mer_id = $this->pay_type_list['mid'];
    }

    /**
     * @desc 余额查询
     */
    public function balance($user_id = '')
    {
        $_url  = 'https://b2b.umfintech.com/merAccess/member/balance';
        if ($user_id == ''){
            $user_id = $this->pay_type_list['mid'];
        }
        $data = [
            'mer_id'    => $this->pay_type_list['mid'],
            'user_id'   => $user_id,
            'version'   => '1.0',

        ];
        return Util::httpGet($data,$_url);
    }
    /**
     * @desc 账户余额提现
     */
    public function balanceWithdrawal($user_id = '')
    {
        if ($user_id == ''){
            $user_id = '51328';
        }
        $_url  = 'https://b2b.umfintech.com/merAccess/withdrawal/balanceWithdrawal';
        $data = [
            'mer_id'        => '51328',
            'notify_url'    => '51328',
            'user_id'       => $user_id,
            'version'       => '1.1',
            'start_date'    => '20230111',
            'end_date'      => '20230511',
            'page_num'      => '1',
            'page_size'     => '100',

        ];
        return Util::httpGet($data,$_url);
    }

    /**
     * @desc 个人子商户注册
     */
    public function personal()
    {
        $_url = 'https://b2b.umfintech.com/merAccess/register/personal';

        $data = [
            'mer_id'=> $this->mer_id,
            'notify_url'=> 'https://test.com',
            'version'=> '1.0',
            'order_id'=> 'UMFARP'.time(),
            'mer_date'=> date('Ymd'),
            'mer_cust_id'=> '9527',
            'user_type'=> '1',
            'mobile_id'=> '18888888888',
            'cust_name'=> Util::encrypt('张志超'),
            'identity_type'=> '1',
            'identity_code'=> Util::encrypt('210921199004018816'),
            'sub_mer_alias'=> '个人子账号测试',
        ];
        return Util::httpPost($data,$_url);
    }
}