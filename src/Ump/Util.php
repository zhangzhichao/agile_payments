<?php
namespace AgilePayments\Ump;

use AgilePayments\Tool;

class Util
{

    /**
     * @author zhangZC
     * @creationtime 2022年10月28日
     * @desc Post->json 请求
     */
    public static function curlPost($url,$params = [], $type = 'json', $header = [])
    {
        $head = [];
        if ($type == 'json'){
            $head = ['Content-Type: application/json'];
        }
        foreach ($header as $v){
            $head[] = $v;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        // POST数据
        curl_setopt($ch, CURLOPT_POST, 1);
        // 把post的变量加上
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output,true);

    }
}