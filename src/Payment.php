<?php

namespace AgilePayments;

use app\api\controller\UMFintech;
use Exception;
use Alipay\EasySDK\Kernel\Factory;
use Alipay\EasySDK\Kernel\Util\ResponseChecker;
use AgilePayments\Config;

/**
 * @method array    pay(string $order_no, int $pay_price, string $out_time, string $notify_url, array $extra_param = [], string $title = '') 支付
 * @method array    getPayInfo($trade_no,$order_no,$type,$amount,$openid,$notify) 联动获取支付信息
 * @method array    query(string $trade_no, string $date = '')   查询方法[云闪付必填时间]
 * @method array    refund(string $out_order_id, int $pay_price)  退款方法
 * @method array    refundQuery(string $refund_no, string $trade_no, string $date = '') 退款查询方法
 * @method bool     close(string $trade_no)   关闭支付方法
 * @method bool     verifySign($request)  验签方法
 */
class Payment
{
    private $pay_config;

    private $platform;

    private $obj;
    
    public function __construct(Config $config)
    {
        $arr = [
            'umf'       => 'UMFintech',     // 联动
            'union'     => 'Union',         // 云闪付
            'ump'       => 'Ump',           // 银商
            'alipay'    => 'Alipay',        // 支付宝
            'wechat'    => 'Wechat',        // 微信
            'ipay'      => 'Apple',        // 微信
        ];
        $this->platform = $arr[strtolower($config->pay_type_config->platform)];
        $this->pay_config = $config;
    }

    public function __call($name, $arguments)
    {
        $class = "\AgilePayments\\{$this->platform}\Service";
        if (method_exists($class,$name)){
            return call_user_func_array([new $class($this->pay_config),$name],$arguments);
        }else{
            throw new \Exception('调用方法错误！');
        }
    }
}